import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';

import Routes from './Routes';
import reducers from './reducers';


class Main extends Component {
    
    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyAEhbiXI2yfWU9O5qyRK1K_UnVZjo93L2I',
            authDomain: 'entregue-85262.firebaseapp.com',
            databaseURL: 'https://entregue-85262.firebaseio.com',
            projectId: 'entregue-85262',
            storageBucket: 'entregue-85262.appspot.com',
            messagingSenderId: '701557782096'
        });
    }

    render() {
        return (
            <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
                <Routes />
            </Provider>
        );
    }
}

export default Main;
