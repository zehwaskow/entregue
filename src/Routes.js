import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import FormLogin from './components/FormLogin';
import Drawer from './components/Drawer';
import HomeScreen from './components/HomeScreen';
import UserRegisterForm from './components/UserRegisterForm';
import CustomerRegisterForm from './components/CustomerRegisterForm';
import CompanyResgisterTab from './components/CompanyRegisterTab';
import CompanyManagerRegisterForm from './components/CompanyManagerRegisterForm';
import WelcomeScreen from './components/WelcomeScreen';

const Routes = () => (
    <Router hideNavBar >
        <Scene key='root'>
        <Scene key='welcomeScreen' component={WelcomeScreen} hideNavBar /> 
        <Scene key='companyManagerRegister' component={CompanyManagerRegisterForm} hideNavBar /> 
        <Scene key='welcome' component={HomeScreen} title='Home' hideNavBar />
          <Scene key='login' component={FormLogin} initial hideNavBar />  
          <Scene key='drawer' component={Drawer} hideNavBar open={false} >
            <Scene key='home' component={HomeScreen} title='Home' hideNavBar />
            <Scene key='userRegister' component={UserRegisterForm} hideNavBar />
            <Scene key='customerRegister' component={CustomerRegisterForm} hideNavBar />
            <Scene key='companyRegisterTab' component={CompanyResgisterTab} hideNavBar /> 
          </Scene>    
        </Scene>    
    </Router>     
);

export default Routes;
