import firebase from 'firebase';
import { Actions } from 'react-native-router-flux';
import b64 from 'base-64';

import {
    CHANGE_EMAIL,
    CHANGE_PASSWORD,
    USER_LOGIN_SUCESS,
    USER_LOGIN_ERROR,
    LOGIN_IN_PROGRESS,
} from './types';

export const changeLoginEmail = (text) => {
    return {
        type: CHANGE_EMAIL,
        payload: text
    };
};

export const changeLoginPassword = (text) => {
    return {
        type: CHANGE_PASSWORD,
        payload: text
    };
};

export const authenticateUser = ({ email, password }) => {
    return dispatch => {
    dispatch({ type: LOGIN_IN_PROGRESS });

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(value => userLoginSucess(dispatch))
            .catch(error => userLoginError(error, dispatch));
    };
};

const userLoginSucess = (dispatch) => {
    console.log(dispatch);
    dispatch(
        {
            type: USER_LOGIN_SUCESS
        }
    );
    Actions.drawer();
};

const userLoginError = (error, dispatch) => {
    console.log(error.message);
    dispatch(
        {
            type: USER_LOGIN_ERROR,
            payload: error.message
        }
    );
};
