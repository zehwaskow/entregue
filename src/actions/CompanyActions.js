import firebase, { database } from 'firebase';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import {
    CHANGE_USER_EMAIL,
    CHANGE_COMPANY_EMAIL,
    CHANGE_USER_PASSWORD,
    CHANGE_USER_TYPE,
    CHANGE_USER_NAME,
    CHANGE_EIN,
    CHANGE_STATE_SUBSCRIPTION,
    CHANGE_COMPANY_NAME,
    CHANGE_ZIP_CODE,
    CHANGE_STREET,
    CHANGE_NUMBER,
    CHANGE_ADRESS_LINE_TWO,
    CHANGE_NEIGHBORHOOD,
    CHANGE_CITY,
    CHANGE_STATE,
    CHANGE_LANDLINE_TELEPHONE,
    CHANGE_CELL_PHONE,
    CHANGE_WHATSAPP_PHONE,
    CHANGE_LEGAL_REPRESENTATIVE,
    USER_REGISTER_SUCESS,
    USER_REGISTER_ERROR,
    USER_LOGIN_SUCESS,
    USER_LOGIN_ERROR,
    LOGIN_IN_PROGRESS,
    REGISTER_IN_PROGRESS,

} from '.././actions/types';

export const changeUserEmail = (text) => {
    return {
        type: CHANGE_USER_EMAIL,
        payload: text
    };
};

export const changeCompanyEmail = (text) => {
    return {
        type: CHANGE_COMPANY_EMAIL,
        payload: text
    };
};

export const changeUserPassword = (text) => {
    return {
        type: CHANGE_USER_PASSWORD,
        payload: text
    };
};

export const changeUserType = (text) => {
    return {
        type: CHANGE_USER_TYPE,
        payload: text
    };
};

export const changeUserName = (text) => {
    return {
        type: CHANGE_USER_NAME,
        payload: text
    };
};

export const changeCompanyName = (text) => {
    return {
        type: CHANGE_COMPANY_NAME,
        payload: text
    };
};

export const changeEin = (text) => {
    return {
        type: CHANGE_EIN,
        payload: text
    };
};

export const changeStateSubscription = (text) => {
    return {
        type: CHANGE_STATE_SUBSCRIPTION,
        payload: text
    };
};

export const changeStreet = (text) => {
    return {
        type: CHANGE_STREET,
        payload: text
    };
};

export const changeNumber = (text) => {
    return {
        type: CHANGE_NUMBER,
        payload: text
    };
};

export const changeAdressLineTwo = (text) => {
    return {
        type: CHANGE_ADRESS_LINE_TWO,
        payload: text
    };
};

export const changeNeighborhood = (text) => {
    return {
        type: CHANGE_NEIGHBORHOOD,
        payload: text
    };
};

export const changeZipCode = (text) => {
    return {
        type: CHANGE_ZIP_CODE,
        payload: text
    };
};

export const changeCity = (text) => {
    return {
        type: CHANGE_CITY,
        payload: text
    };
};

export const changeState = (text) => {
    return {
        type: CHANGE_STATE,
        payload: text
    };
};

export const changeLandLineTelephone = (text) => {
    return {
        type: CHANGE_LANDLINE_TELEPHONE,
        payload: text
    };
};

export const changeCellPhone = (text) => {
    return {
        type: CHANGE_CELL_PHONE,
        payload: text
    };
};

export const changeWhatsappPhone = (text) => {
    return {
        type: CHANGE_WHATSAPP_PHONE,
        payload: text
    };
};

export const changeLegalRepresentative = (text) => {
    return {
        type: CHANGE_LEGAL_REPRESENTATIVE,
        payload: text
    };
};

export const addManagerUser = ({ 
                                email, 
                                password, 
                                name, 
                                companyName, 
                                ein, 
                                whatsappPhone, 
                            }) => {
    return dispatch => {
        dispatch({ type: REGISTER_IN_PROGRESS });
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
            const userUid = firebase.auth().currentUser.uid;
            firebase.database().ref(`users/${userUid}`)
            .push({ name, managerUid: userUid, whatsappPhone, userType: 'manager', admin: 'true', ein, companyName })
            .then(value => userRegisterSucess(dispatch));
            console.log('Successfully fetched user data:', user.toJSON());
        })
        .catch(error => userRegisterError(error, dispatch));
        console.log('dispacthErro: ', dispatch);
    };
};  

const userRegisterSucess = (dispatch) => {
    dispatch({ type: USER_REGISTER_SUCESS });

    Actions.welcomeScreen();
};

const userRegisterError = (error, dispatch) => {
    dispatch({ type: USER_REGISTER_ERROR, payload: error.message });
};

export const addCompanyUser = ({ email, 
                                     password, 
                                     name, 
                                     whatsappPhone,
                                     
}) => {

    return dispatch => {
        dispatch({ type: REGISTER_IN_PROGRESS });
        const userUid = firebase.auth().currentUser.uid;
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
            firebase.database().ref(`users/${userUid}`)
            .push({ name, managerUid: userUid, admin: 'false', whatsappPhone, userType: 'employee' })
            .then(value => insertRegisterSucess(dispatch));
            console.log('dispacthSucesso: ', dispatch);
            console.log('UID: ', userUid);
            console.log('Successfully fetched user data:', user.toJSON());
        })
        .catch(error => insertRegisterError(error, dispatch));   
        console.log('dispacthErro: ', dispatch.error);
    };
};  

const insertRegisterSucess = (dispatch) => {
    dispatch({ type: USER_REGISTER_SUCESS });

    Actions.home();
};

const insertRegisterError = (error, dispatch) => {
    console.log(error);
    dispatch({ type: USER_REGISTER_ERROR, payload: error.message });
};

export const addCompany = ({
                            ein,
                            companyName,
                            stateSubscription,
                            companyEmail,
                            cellPhone,
                            whatsappPhone,  
}) => {
    return dispatch => {
        const userUid = firebase.auth().currentUser.uid;
        firebase.database().ref('companies')
        .push({ ein,
                companyName,
                stateSubscription,
                companyEmail,
                cellPhone,
                whatsappPhone,
                managerUid: userUid })
        .then(value => insertRegisterSucess(dispatch))
        .catch(error => insertRegisterError(error, dispatch)); 
        console.log('dispacthErro: ', dispatch);
        };
};
