import firebase, { database } from 'firebase';
import { Actions } from 'react-native-router-flux';
import _ from 'lodash';

import {
    CHANGE_CUSTOMER_EMAIL, 
    CHANGE_CUSTOMER_TYPE,
    CHANGE_CUSTOMER_NAME,
    CHANGE_CUSTOMER_EIN,
    CHANGE_CUSTOMER_STATE_SUBSCRIPTION,
    CHANGE_CUSTOMER_ZIP_CODE, 
    CHANGE_CUSTOMER_STREET, 
    CHANGE_CUSTOMER_NUMBER, 
    CHANGE_CUSTOMER_ADRESS_LINE_TWO,
    CHANGE_CUSTOMER_NEIGHBORHOOD,
    CHANGE_CUSTOMER_CITY,
    CHANGE_CUSTOMER_STATE,
    CHANGE_CUSTOMER_LANDLINE_TELEPHONE,
    CHANGE_CUSTOMER_CELL_PHONE,
    CHANGE_CUSTOMER_WHATSAPP_PHONE,
    CHANGE_CUSTOMER_LEGAL_REPRESENTATIVE,
    CHANGE_CUSTOMER_OBSERVATIONS,
    CUSTOMER_REGISTER_SUCCESS,
    CUSTOMER_REGISTER_ERROR,
    LIST_CUSTOMER,
    CUSTOMER_REGISTER_IN_PROGRESS

} from '.././actions/types';

export const changeCustomerEmail = (text) => {
    return {
        type: CHANGE_CUSTOMER_EMAIL,
        payload: text
    };
};

export const changeCustomerType = (text) => {
    return {
        type: CHANGE_CUSTOMER_TYPE,
        payload: text
    };
};

export const changeCustomerName = (text) => {
    return {
        type: CHANGE_CUSTOMER_NAME,
        payload: text
    };
};

export const changeCustomerEin = (text) => {
    return {
        type: CHANGE_CUSTOMER_EIN,
        payload: text
    };
};

export const changeCustomerStateSubscription = (text) => {
    return {
        type: CHANGE_CUSTOMER_STATE_SUBSCRIPTION,
        payload: text
    };
};

export const changeCustomerStreet = (text) => {
    return {
        type: CHANGE_CUSTOMER_STREET,
        payload: text
    };
};

export const changeCustomerNumber = (text) => {
    return {
        type: CHANGE_CUSTOMER_NUMBER,
        payload: text
    };
};

export const changeCustomerAdressLineTwo = (text) => {
    return {
        type: CHANGE_CUSTOMER_ADRESS_LINE_TWO,
        payload: text
    };
};

export const changeCustomerNeighborhood = (text) => {
    return {
        type: CHANGE_CUSTOMER_NEIGHBORHOOD,
        payload: text
    };
};

export const changeCustomerZipCode = (text) => {
    return {
        type: CHANGE_CUSTOMER_ZIP_CODE,
        payload: text
    };
};

export const changeCustomerCity = (text) => {
    return {
        type: CHANGE_CUSTOMER_CITY,
        payload: text
    };
};

export const changeCustomerState = (text) => {
    return {
        type: CHANGE_CUSTOMER_STATE,
        payload: text
    };
};

export const changeCustomerLandLineTelephone = (text) => {
    return {
        type: CHANGE_CUSTOMER_LANDLINE_TELEPHONE,
        payload: text
    };
};

export const changeCustomerCellPhone = (text) => {
    return {
        type: CHANGE_CUSTOMER_CELL_PHONE,
        payload: text
    };
};

export const changeCustomerWhatsappPhone = (text) => {
    return {
        type: CHANGE_CUSTOMER_WHATSAPP_PHONE,
        payload: text
    };
};

export const changeCustomerLegalRepresentative = (text) => {
    return {
        type: CHANGE_CUSTOMER_LEGAL_REPRESENTATIVE,
        payload: text
    };
};

export const addCustomer = ({ customerEin, 
                              customerName, 
                              customerStateSubscription, 
                              customerEmail, 
                              customerCellPhone, 
                              customerWhatsappPhone, 
}) => {

return dispatch => {
    dispatch({ type: CUSTOMER_REGISTER_IN_PROGRESS });
    const userUid = firebase.auth().currentUser.uid;
    firebase.database().ref(`customers/${userUid}`)
    .set({ customerEin, 
            customerName, 
            customerStateSubscription, 
            customerEmail, 
            customerCellPhone, 
            customerWhatsappPhone, 
            managerUid: userUid 
        })
.then(value => insertRegisterSucess(dispatch))
.catch(error => insertRegisterError(error, dispatch)); 
console.log('dispacthErro: ', dispatch);
};
};


const insertRegisterSucess = (dispatch) => {
    dispatch({ type: CUSTOMER_REGISTER_SUCCESS });

    Actions.drawer();
};

const insertRegisterError = (error, dispatch) => {
    dispatch({ type: CUSTOMER_REGISTER_ERROR, payload: error.message });
};

export const customersFetch = () => {
return dispatch => {

    firebase.database('customers')
        .once('value', snapshot => {
            dispatch({ type: LIST_CUSTOMER, payload: snapshot.val() });
            console.log(snapshot);
        });
};
};
