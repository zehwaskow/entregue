import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, Text, StatusBar, KeyboardAvoidingView } from 'react-native';
import { Container, Item, Input, Icon, Button, Header, Title, Content, Spinner } from 'native-base';
import Expo from 'expo';
import { 
    addManagerUser, 
    addCompanyUser,
    changeUserEmail, 
    changeUserPassword,
    changeUserType, 
    changeUserName,
    changeCompanyName,
    changeEin,
    changeWhatsappPhone

} from '../actions/CompanyActions';

console.disableYellowBox = true;

class CompanyManagerRegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false, userType: 'manager' });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
        this.setState({ userType: 'manager' });
     }
     
    _addManagerUser() {
        const { email, password, name, companyName, ein, whatsappPhone } = this.props;

        this.props.addManagerUser({ email, password, name, companyName, ein, whatsappPhone });
    }
    _addCompanyUser() {
        const { userType, email, password, name, companyName, ein, whatsappPhone } = this.props;

        this.props.addCompanyUser({ userType, email, password, name, companyName, ein, whatsappPhone });
    }

    renderBtnRegister() {
        if (this.props.loading_register) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }

        return (
            <Button 
                full 
                rounded 
                light 
                style={{ padding: 25 }} 
                onPress={() => { this._addManagerUser(); }}
            >
        <Text>Salvar</Text>
    </Button>
        );
    }


    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ height: null, width: null, flex: 1 }}
            >
            <Content style={{ flex: 1 }}>
            <KeyboardAvoidingView behavior="padding">
                <Container style={{ flex: 1 }}>
                    <StatusBar hidden />
                    <Header><Title>Cadastrar Usuário</Title></Header>
                    <Container style={{ justifyContent: 'center', flex: 4, marginHorizontal: 7 }}>
                        <Item rounded style={{ marginBottom: 15, marginTop: 15 }} >
                        <Icon active name='paper' />
                            <Input 
                                placeholder='CNPJ' 
                                value={this.props.ein}
                                onChangeText={text => this.props.changeEin(text)}
                            />
                        </Item>
                        <Item rounded style={{ marginBottom: 15 }} >
                        <Icon active name='paper' />
                            <Input 
                                placeholder='Razão Social' 
                                value={this.props.companyName}
                                onChangeText={text => this.props.changeCompanyName(text)}
                            />
                        </Item>    
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='smartphone' />
                            <Input 
                                placeholder='Telefone Whatsapp' 
                                value={this.props.whatsappPhone}
                                onChangeText={text => this.props.changeWhatsappPhone(text)}
                            />
                            </Item>
                        <Item rounded style={{ marginBottom: 15 }} >
                        <Icon active name='mail' />
                            <Input 
                                placeholder='E-mail' 
                                value={this.props.email}
                                onChangeText={text => this.props.changeUserEmail(text)}
                            />
                        </Item>  
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='person' />
                            <Input 
                                placeholder='Nome Usuário' 
                                value={this.props.name}
                                onChangeText={text => this.props.changeUserName(text)}
                            />

                        </Item>
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='lock' />
                            <Input 
                                placeholder='Senha' 
                                value={this.props.password}
                                onChangeText={text => this.props.changeUserPassword(text)}
                            />
                        </Item>
                        <Container 
                            style={{ 
                                alignItems: 'center', 
                                justifyContent: 'space-between', 
                                flex: 1, 
                                marginHorizontal: 25, 
                                marginBottom: 20 }}
                        >    
                           {this.renderBtnRegister()}
                        </Container>
                    </Container>
                </Container>  
                </KeyboardAvoidingView>
                </Content>     
        </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    
    return (
        {
            ein: state.CompanyReducer.ein,
            email: state.CompanyReducer.email,
            password: state.CompanyReducer.password,
            userType: state.CompanyReducer.userType,
            name: state.CompanyReducer.name,
            companyName: state.CompanyReducer.companyName,   
            whatsappPhone: state.CompanyReducer.whatsappPhone,
            loading_register: state.CompanyReducer.loading_register,         
        }
    );
};

export default connect(
    mapStateToProps, 
        {
            addManagerUser, 
            addCompanyUser,
             changeWhatsappPhone,
            changeUserEmail, 
            changeEin,
            changeUserPassword, 
            changeUserType,
            changeUserName,
            changeCompanyName
        })(CompanyManagerRegisterForm);
