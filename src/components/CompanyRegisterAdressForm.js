import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, Text, StatusBar, KeyboardAvoidingView } from 'react-native';
import { Container, Item, Input, Icon, Button, Spinner, Content } from 'native-base';
import Expo from 'expo';

import {
        addCompany,
        changeZipCode,
        changeStreet,
        changeNumber,
        changeAdressLineTwo,
        changeNeighborhood,
        changeCity,
        changeState  
    } from '../actions/CompanyActions';

console.disableYellowBox = true;

class CompanyRegisterAdressForm extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
     }

     _addCompany() {
        const { 
                ein,
                companyName,
                stateSubscription,
                companyEmail,
                cellPhone,
                whatsappPhone,
                zipCode, 
                street, 
                number, 
                adressLineTwo,
                neighborhood,
                city,
                state 

            } = this.props;

        this.props.addCompany({  
            ein,
            companyName,
            stateSubscription,
            companyEmail,
            cellPhone,
            whatsappPhone,
                zipCode, 
                street, 
                number, 
                adressLineTwo,
                neighborhood,
                city,
                state
        });
    }

     renderBtnRegister() {
        if (this.props.loading_register) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }

        return (
            <Button 
                full 
                rounded 
                light 
                style={{ padding: 25 }} 
                onPress={() => { this._addCompany(); }}
            >
        <Text>Salvar</Text>
    </Button>
        );
    }
     
    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
             
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ height: null, width: null, flex: 1 }}
            >
            <Content style={{ flex: 1 }}>
              <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding"> 
                <Container style={{ flex: 1 }}>
                        <Item style={{ marginBottom: 15, marginTop: 25 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='CEP' 
                                value={this.props.zipCode}
                                onChangeText={text => this.props.changeZipCode(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='Rua' 
                                value={this.props.street}
                                onChangeText={text => this.props.changeStreet(text)}
                            />
                            </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='Número' 
                                value={this.props.number}
                                onChangeText={text => this.props.changeNumber(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='Complemento' 
                                value={this.props.adressLineTwo}
                                onChangeText={text => this.props.changeAdressLineTwo(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='Bairro' 
                                value={this.props.neighborhood}
                                onChangeText={text => this.props.changeNeighborhood(text)}
                            />
                        </Item>
                         <Item style={{ marginBottom: 15 }} >
                            <Icon active name='location-city' />
                            <Input 
                                placeholder='Cidade' 
                                value={this.props.city}
                                onChangeText={text => this.props.changeCity(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='map' />
                            <Input 
                                placeholder='Estado' 
                                value={this.props.state}
                                onChangeText={text => this.props.changeState(text)}
                            />
                        </Item>
                        <Container 
                            style={{ 
                                alignItems: 'center', 
                                justifyContent: 'flex-end', 
                                flex: 1, 
                                marginHorizontal: 5, 
                                marginBottom: 50 }}
                        >    
                           {this.renderBtnRegister()}
                        </Container>     
                </Container>  
                </KeyboardAvoidingView>
                </Content>
        </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    return (
        {
            ein: state.CompanyReducer.ein,
            email: state.CompanyReducer.email,
            password: state.CompanyReducer.password,
            userType: state.CompanyReducer.userType,
            name: state.CompanyReducer.name,
            companyName: state.CompanyReducer.companyName,   
            whatsappPhone: state.CompanyReducer.whatsappPhone,    
            zipCode: state.CompanyReducer.zipCode,
            street: state.CompanyReducer.street,
            number: state.CompanyReducer.number,
            adressLineTwo: state.CompanyReducer.adressLineTwo,
            neighborhood: state.CompanyReducer.neighborhood,
            city: state.CompanyReducer.city,
            state: state.CompanyReducer.state,
        }
    );
};

export default connect(
    mapStateToProps, 
        {   
            addCompany,
            changeZipCode,
            changeStreet,
            changeNumber,
            changeAdressLineTwo,
            changeNeighborhood,
            changeCity,
            changeState  
        }

    )(CompanyRegisterAdressForm);
