import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, Text, StatusBar, KeyboardAvoidingView } from 'react-native';
import { Container, Item, Input, Icon, Button, Header, Title, Content, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Expo from 'expo';

import { 
    addCompany,
    changeEin,
    changeCompanyName,
    changeStateSubscription,
    changeCompanyEmail,
    changeCellPhone,
    changeWhatsappPhone,
    } from '../actions/CompanyActions';

console.disableYellowBox = true;

class CompanyRegisterInitialForm extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
     }

     componentDidMount() {
        StatusBar.setHidden(true);
     }

     _addCompany() {
        const { 
                ein,
                companyName,
                stateSubscription,
                companyEmail,
                cellPhone,
                whatsappPhone,

            } = this.props;

        this.props.addCompany({  
            ein,
            companyName,
            stateSubscription,
            companyEmail,
            cellPhone,
            whatsappPhone,
        });
    }

     renderBtnRegister() {
        if (this.props.loading_register) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }

        return (
            <Button 
                full 
                rounded 
                info 
                style={{ padding: 25 }} 
                onPress={() => { this._addCompany(); }}
            >
        <Text>Salvar</Text>
    </Button>
        );
    }

     
    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
             
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ height: null, width: null, flex: 1 }}
            >
            
            <Content style={{ flex: 1 }}>
              <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding"> 
                    <Container style={{ flex: 1 }}>
                    <Item style={{ marginBottom: 15, marginTop: 25 }} >
                            <Icon active name='paper' />
                            <Input 
                                placeholder='CNPJ' 
                                value={this.props.ein}
                                onChangeText={text => this.props.changeEin(text)}
                            />
                        </Item>  
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='person' />
                            <Input 
                                placeholder='Razão Social' 
                                value={this.props.companyName}
                                onChangeText={text => this.props.changeCompanyName(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='paper' />
                            <Input 
                                placeholder='Inscrição Estadual' 
                                value={this.props.stateSubscription}
                                onChangeText={text => this.props.changeStateSubscription(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='mail' />
                            <Input 
                                placeholder='E-mail da Empresa' 
                                value={this.props.companyEmail}
                                onChangeText={text => this.props.changeCompanyEmail(text)}
                            />
                        </Item> 
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='smartphone' />
                            <Input 
                                placeholder='Celular' 
                                value={this.props.cellPhone}
                                onChangeText={text => this.props.changeCellPhone(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='smartphone' />
                            <Input 
                                placeholder='Telefone Whatsapp' 
                                value={this.props.whatsappPhone}
                                onChangeText={text => this.props.changeWhatsappPhone(text)}
                            />
                            </Item>
                            <Container 
                            style={{ 
                                alignItems: 'center', 
                                justifyContent: 'flex-end', 
                                flex: 1, 
                                marginHorizontal: 5, 
                                marginBottom: 50 }}
                            >    
                           {this.renderBtnRegister()}
                        </Container> 
                </Container>  
                </KeyboardAvoidingView>
                </Content>
        </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    
    return (
        {
            ein: state.CompanyReducer.ein,
            companyName: state.CompanyReducer.companyName,
            stateSubscription: state.CompanyReducer.stateSubscription,
            companyEmail: state.CompanyReducer.companyEmail,   
            cellPhone: state.CompanyReducer.cellPhone,
            whatsappPhone: state.CompanyReducer.whatsappPhone,
            loading_register: state.CompanyReducer.loading_register,         
        }
    );
};

export default connect(
    mapStateToProps, 
        {
            addCompany,
            changeEin,
            changeCompanyName,
            changeStateSubscription,
            changeCompanyEmail,
            changeCellPhone,
            changeWhatsappPhone,
        })(CompanyRegisterInitialForm);
