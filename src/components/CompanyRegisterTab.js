import * as React from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import { TabViewAnimated, TabBar, SceneMap } from 'react-native-tab-view';

import CompanyRegisterAdressForm from './CompanyRegisterAdressForm';
import CompanyRegisterInitialForm from './CompanyRegisterInitialForm';

const initialLayout = {
  height: 0,
  width: Dimensions.get('window').width,
};

export default class CompanyResgisterTab extends React.Component {
  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Dados Básicos' },
      { key: 'second', title: 'Endereço' },
    ],
  };

  _handleIndexChange = index => this.setState({ index });

  _renderHeader = props => <TabBar {...props} />;

  _renderScene = SceneMap({
    first: CompanyRegisterInitialForm,
    second: CompanyRegisterAdressForm,
  });

  render() {
    return (
      <TabViewAnimated
        navigationState={this.state}
        renderScene={this._renderScene}
        renderHeader={this._renderHeader}
        onIndexChange={this._handleIndexChange}
        initialLayout={initialLayout}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
