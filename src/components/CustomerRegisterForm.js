import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, Text, StatusBar, KeyboardAvoidingView } from 'react-native';
import { Container, Item, Input, Icon, Button, Header, Title, Content, Spinner } from 'native-base';
import Expo from 'expo';
import { 
    addCustomer, 
    changeCustomerEin,
    changeCustomerName,
    changeCustomerStateSubscription,
    changeCustomerEmail,
    changeCustomerCellPhone,
    changeCustomerWhatsappPhone,

} from '../actions/CustomerActions';

console.disableYellowBox = true;

class CustomerRegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
     }
     
    _addCustomer() {
        const { customerEin, 
                customerName, 
                customerStateSubscription, 
                customerEmail, 
                customerCellPhone, 
                customerWhatsappPhone, 
        } = this.props;

        this.props.addCustomer({ customerEin, 
                                 customerName, 
                                 customerStateSubscription, 
                                 customerEmail, 
                                 customerCellPhone, 
                                 customerWhatsappPhone, 
        });
    }

    renderBtnRegisterCustomer() {
        if (this.props.loading_customer_register) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }

        return (
            <Button 
                full 
                rounded 
                light 
                style={{ padding: 25 }} 
                onPress={() => { this._addCustomer(); }}
            >
        <Text>Salvar</Text>
    </Button>
        );
    }


    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ height: null, width: null, flex: 1 }}
            >
            <Content style={{ flex: 1 }}>
            <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                <Container style={{ flex: 1 }}>
                    <StatusBar hidden />
                    <Header style={{ backgroundColor: '#e8e8e8' }}><Title style={{ text: '#000000' }}>Cadastrar Cliente</Title></Header>
                    <Container style={{ flex: 5 }}>
                    <Item style={{ marginBottom: 15, marginTop: 25 }} >
                            <Icon active name='paper' />
                            <Input 
                                placeholder='CNPJ' 
                                value={this.props.customerEin}
                                onChangeText={text => this.props.changeCustomerEin(text)}
                            />
                        </Item>  
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='person' />
                            <Input 
                                placeholder='Razão Social' 
                                value={this.props.customerName}
                                onChangeText={text => this.props.changeCustomerName(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='paper' />
                            <Input 
                                placeholder='Inscrição Estadual' 
                                value={this.props.customerStateSubscription}
                                onChangeText={text => this.props.changeCustomerStateSubscription(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='mail' />
                            <Input 
                                placeholder='E-mail da Empresa' 
                                value={this.props.customerEmail}
                                onChangeText={text => this.props.changeCustomerEmail(text)}
                            />
                        </Item> 
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='smartphone' />
                            <Input 
                                placeholder='Celular' 
                                value={this.props.customerCellPhone}
                                onChangeText={text => this.props.changeCustomerCellPhone(text)}
                            />
                        </Item>
                        <Item style={{ marginBottom: 15 }} >
                            <Icon active name='smartphone' />
                            <Input 
                                placeholder='Telefone Whatsapp' 
                                value={this.props.customerwhatsappPhone}
                                onChangeText={text => this.props.changeCustomerWhatsappPhone(text)}
                            />
                            </Item>
                        <Container 
                            style={{ 
                                alignItems: 'center', 
                                justifyContent: 'space-between', 
                                flex: 1, 
                                marginHorizontal: 25, 
                                marginBottom: 20 }}
                        >    
                           {this.renderBtnRegisterCustomer()}
                        </Container>
                    </Container>
                </Container>  
                </KeyboardAvoidingView>
                </Content>     
        </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    
    return (
        {
            customerEin: state.CustomerReducer.customerEin,
            customerName: state.CustomerReducer.customerName,
            customerStateSubscription: state.CustomerReducer.customerStateSubscription,
            customerEmail: state.CustomerReducer.customerEmail,   
            customerCellPhone: state.CustomerReducer.customerCellPhone,
            customerWhatsappPhone: state.CustomerReducer.customerWhatsappPhone,
            loading_customer_register: state.CustomerReducer.loading_customer_register,         
        }
    );
};

export default connect(
    mapStateToProps, 
        { 
            addCustomer, 
            changeCustomerEin,
            changeCustomerName,
            changeCustomerStateSubscription,
            changeCustomerEmail,
            changeCustomerCellPhone,
            changeCustomerWhatsappPhone,
            
        })(CustomerRegisterForm);
