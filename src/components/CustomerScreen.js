import React, { Component } from 'react';
import firebase, { database } from 'firebase';
import { connect } from 'react-redux';
import { TouchableHighlight, View, ListView, StyleSheet } from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body } from 'native-base';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';

import { 
    customersFetch,

} from '../actions/CompanyActions';

export default class CustomerScreen extends Component {

    constructor() {
        super();
        let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

        this.state = {
            usersDataSource: ds 
        };

        this.usersRef = this.getRef().child('users');

        this.renderRow = this.renderRow.bind(this);
        this.pressRow = this.pressRow.bind(this);
    }

    getRef() {
        return firebase.database().ref();
    }
   
    componentWillMount() {
        this.getItems(this.usersRef);
    }

    componentDidMount() {
        this.getItems(this.usersRef);
    }

    getItems(usersRef) {
        //let items = [{ title: 'Item One' }, { title: 'Item Two' }];
        
        usersRef.on('value', (snap) => {
            const users = [];
            snap.forEach((child) => {
                users.push({
                    name: child.val().name,
                    userType: child.val().userType,
                    ein: child.val().ein,
                    managerUid: child.val().managerUid,
                    companyName: child.val().companyName,

                });
            });
            this.setState({
                usersDataSource: this.state.usersDataSource.cloneWithRows(users)
            });
            console.log(users);
        });  
    }


    pressRow(user) {
        console.log(user);
    }

    renderRow(user) {
        return (
            <TouchableHighlight 
                onPress={() => {
                this.pressRow(user);
          }}> 
                <View style={styles.item}>
                    <Text style={styles.txtValor}>Nome do Usuário: {user.name}</Text>
                    <Text style={styles.txtValor}>CNPJ: {user.ein}</Text>
                    <Text style={styles.txtValor}>Id do Gestor:{user.managerUid}</Text>
                    <Text style={styles.txtValor}>Nome da Empresa: {user.companyName}</Text>
                </View>    
            </TouchableHighlight>
        );
    }


    render() {
        return (
            <View style={styles.item}>
                <ListView 
                    dataSource={this.state.usersDataSource}
                    renderRow={this.renderRow}
                />
            </View>    
        );
    }
}


const styles = StyleSheet.create({
    principal: {
        flex: 1
    },
    item: {
      backgroundColor: 'transparent',
      borderWidth: 0.5,
      borderColor: '#999',
      margin: 10,
      padding: 10,
      flexDirection: 'column'
    },
    detalhesItem: {
        marginLeft: 20,
        flex: 1
    
      },
      txtTitulo: {
        fontSize: 18,
        color: 'blue',
        marginBottom: 5
      },
      txtValor: {
        fontSize: 16,
        fontWeight: 'bold'
      },
      txtDetalhes: {
        fontSize: 16
      },
      tituloPrincipal: {
        fontSize: 32
      }
});
