import React from 'react';
import { ImageBackground, Text, StatusBar } from 'react-native';
import { DrawerNavigator } from 'react-navigation';

import HomeScreen from './HomeScreen';
import UserRegisterForm from './UserRegisterForm';
import CustomerRegisterForm from './CustomerRegisterForm';
import CustomerScreen from './CustomerScreen';
import CompanyResgisterTab from './CompanyRegisterTab';


export default class Drawer extends React.Component {
  render() {
    return (
      <ImageBackground
      source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
      style={{ height: null, width: null, flex: 1 }}
      >
          <MyDrawer />
      </ImageBackground>      
    );
  }
}

const MyDrawer = DrawerNavigator({
  Home: {
      screen: HomeScreen
  },
  CadastrarUsuario: {
      screen: UserRegisterForm
  },
  CadastrarCliente: {
      screen: CustomerRegisterForm
  },
  Clientes: {
    screen: CustomerScreen
  },
  CadastrarEmpresa: {
    screen: CompanyResgisterTab
  }
});

