import React, { Component } from 'react';
import { ImageBackground, Image, Text, View, KeyboardAvoidingView, } from 'react-native';
import { Container, Item, Input, Icon, Button, Spinner, Content } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { changeLoginEmail, changeLoginPassword, authenticateUser } from '../actions/AuthenticationActions';

console.disableYellowBox = true;

class FormLogin extends Component {

    _authenticateUser() {
        const { email, password } = this.props;

        this.props.authenticateUser({ email, password });
    }

    renderBtnLogin() {
        if (this.props.loading_login) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }
        return (
            <Button 
                full 
                rounded 
                light 
                style={{ padding: 25 }} 
                onPress={() => this._authenticateUser()}
            >
            <Text>Entrar</Text>
        </Button>
        );
    }

    render() {
        return (
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ flex: 1 }}
            >
            <Content style={{ flex: 1 }}>
            <KeyboardAvoidingView behavior="padding">
                        <Container style={{ flex: 1 }}>
                            <Container 
                                style={{ 
                                    flex: 2, 
                                    justifyContent: 'center', 
                                    alignItems: 'center' }}
                            >
                                <Image 
                                    source={{ uri: 'https://1.bp.blogspot.com/-4wRbegq7oFA/WM_PdJdqifI/AAAAAAAAIhM/9r62UseZE0E4SWyLbePd5L71VTsjS8KuQCLcB/s1600/kurir.png' }}
                                    style={{ height: 120, width: 130 }}
                                />
                                <Text 
                                    style={{ 
                                        fontSize: 35, 
                                        fontWeight: 'bold', 
                                        fontFamily: 'sans-serif', 
                                        color: '#2F4F4F' }}
                                >
                                    Entregue
                                </Text>
                            </Container>
                            <Container 
                                style={{ 
                                    justifyContent: 'center', 
                                    flex: 3, 
                                    marginTop: 45,
                                    marginHorizontal: 7 }}
                            >
                            
                                <Item rounded style={{ marginBottom: 25 }} >
                                    <Icon active name='mail' />
                                    <Input 
                                        placeholder='E-mail' 
                                        onChangeText={text => this.props.changeLoginEmail(text)} 
                                    />
                                </Item>  
                                <Item rounded style={{ marginBottom: 10 }} >
                                    <Icon active name='lock' />
                                    <Input 
                                        placeholder='Senha' 
                                        onChangeText={text => this.props.changeLoginPassword(text)}
                                    />
                                        
                                </Item>
                                <Button transparent success style={{ paddingVertical: 8, marginLeft: 8, marginBottom: 25, justifyContent: 'space-around' }}>
                                    <Text style={{ fontSize: 13 }}>Esqueci a senha</Text>
                                </Button>   
                            </Container>
                            <Container style={{ flex: 2 }}>  
                                <Container 
                                    style={{ 
                                        alignItems: 'center', 
                                        justifyContent: 'flex-end', 
                                        flex: 2, 
                                        marginHorizontal: 25, 
                                        marginBottom: 20 }}
                                >    
                                    {this.renderBtnLogin()}
                                    <Container 
                                        style={{
                                            flex: 1,
                                            justifyContent: 'center',
                                            paddingVertical: 25,
                                            flexDirection: 'row' }}
                                    >
                                        <Button 
                                            transparent 
                                            Primary
                                            style={{ marginLeft: 10, marginTop: -15 }}
                                            onPress={() => { Actions.companyManagerRegister(); }}
                                        >
                                            <Text 
                                                style={{ fontSize: 13 }}
                                            >
                                                Não tem uma conta? Cadastre-se
                                            </Text>
                                        </Button>
                                    </Container>
                                </Container>
                            </Container>
                        </Container>  

            </KeyboardAvoidingView >
            </Content>
            </ImageBackground>
        );
    }
}


const mapStateToProps = state => {
    console.log(state);
    return (
        {
            email: state.AuthenticationReducer.email,
            password: state.AuthenticationReducer.password,
            loading_login: state.AuthenticationReducer.loading_login,
        }
    );
};

export default connect(
    mapStateToProps, 
        { 
            changeLoginEmail, 
            changeLoginPassword, 
            authenticateUser 
        }
    )(FormLogin);
