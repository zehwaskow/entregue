import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, Text, StatusBar } from 'react-native';
import { Container, Item, Input, Icon, Button, Header, Title, Content, Spinner } from 'native-base';
import Expo from 'expo';

import { 
    addCompanyUser,
    changeUserEmail, 
    changeUserPassword,
    changeUserType, 
    changeUserName,
    changeEin,
    changeWhatsappPhone

} from '../actions/CompanyActions';

console.disableYellowBox = true;

class UserRegisterForm extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
     }

     _addCompanyUser() {
        const { userType, email, password, name, companyName, ein, whatsappPhone } = this.props;

        this.props.addCompanyUser({ userType, email, password, name, companyName, ein, whatsappPhone });
    }

    renderBtnRegister() {
        if (this.props.loading_register) {
            return (
                <Spinner size='large' color='blue' />
            );   
        }

        return (
            <Button 
                full 
                rounded 
                light 
                style={{ padding: 25 }} 
                onPress={() => { this._addCompanyUser(); }}
            >
        <Text>Salvar</Text>
    </Button>
        );
    }
     
    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ height: null, width: null, flex: 1 }}
            >
                <Container style={{ flex: 1 }}>
                    <Content>
                    <StatusBar hidden />
                    <Header><Title>Cadastrar Usuário</Title></Header>
                    <Container style={{ justifyContent: 'center', flex: 4, marginHorizontal: 7 }}>
                        <Item rounded style={{ marginBottom: 15, marginTop: 15 }} >
                            <Icon active name='person' />
                            <Input 
                                placeholder='Nome Usuario' 
                                value={this.props.name}
                                onChangeText={text => this.props.changeUserName(text)}
                            />
                        </Item>  
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='cellphone' />
                            <Input 
                                placeholder='Celular' 
                                value={this.props.whatsappPhone}
                                onChangeText={text => this.props.changeWhatsappPhone(text)}
                            />
                        </Item> 
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='mail' />
                            <Input 
                                placeholder='E-mail' 
                                value={this.props.email}
                                onChangeText={text => this.props.changeUserEmail(text)}
                            />
                        </Item>
                        <Item rounded style={{ marginBottom: 15 }} >
                            <Icon active name='lock' />
                            <Input 
                                placeholder='Senha' 
                                value={this.props.password}
                                onChangeText={text => this.props.changeUserPassword(text)}
                            />
                        </Item>
                        <Container 
                            style={{ 
                                alignItems: 'center', 
                                justifyContent: 'flex-end', 
                                flex: 1, 
                                marginHorizontal: 25, 
                                marginBottom: 20 }}
                        >    
                            {this.renderBtnRegister()}
                        </Container>
                    </Container>
                    </Content>    
                </Container>  
        </ImageBackground>
        );
    }
}

const mapStateToProps = state => {
    console.log(state);
    
    return (
        {
            email: state.CompanyReducer.email,
            password: state.CompanyReducer.password,
            userType: state.CompanyReducer.userType,
            name: state.CompanyReducer.name,
            companyName: state.CompanyReducer.companyName,   
            whatsappPhone: state.CompanyReducer.whatsappPhone,
            loading_register: state.CompanyReducer.loading_register,         
        }
    );
};

export default connect(
    mapStateToProps, 
        {
            addCompanyUser,
             changeWhatsappPhone,
            changeUserEmail, 
            changeEin,
            changeUserPassword, 
            changeUserType,
            changeUserName,
        })(UserRegisterForm);
