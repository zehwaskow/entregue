import React, { Component } from 'react';
import { ImageBackground, Image, Text, StatusBar } from 'react-native';
import { Container, Header, Title, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import Expo from 'expo';

export default class WelcomeScreen extends Component {

    constructor(props) {
        super(props);
        this.state = { loading: true };
      }

    async componentWillMount() {
        await Expo.Font.loadAsync({
          Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),

        });

        this.setState({ loading: false });
      }

    componentDidMount() {
        StatusBar.setHidden(true);
     }
     
    render() {
        if (this.state.loading) {
            return <Expo.AppLoading />;
          }
        return (
            <ImageBackground
            source={{ uri: 'https://wallpaperscraft.com/image/color_background_surface_solid_18481_540x960.jpg' }} 
            style={{ width: null, flex: 1 }}
            >
                <Header><Title>Bem-Vindo!</Title></Header>
                <Container style={{ flex: 1 }}>
                <Container style={{ flex: 3, justifyContent: 'flex-start', alignItems: 'center' }}>
                            <Image 
                                source={{ uri: 'https://1.bp.blogspot.com/-4wRbegq7oFA/WM_PdJdqifI/AAAAAAAAIhM/9r62UseZE0E4SWyLbePd5L71VTsjS8KuQCLcB/s1600/kurir.png' }}
                                style={{ height: 180, width: 200 }}
                            />
                            <Text 
                                style={{ 
                                    marginHorizontal: 10,
                                    marginBottom: 45,
                                    fontSize: 35,
                                    padding: 25, 
                                    fontWeight: 'bold', 
                                    fontFamily: 'sans-serif', 
                                    color: '#2F4F4F' }}
                            >
                                Seja bem-vindo ao Entregue!
                            </Text>
                            <Text 
                                style={{
                                    marginTop: -35, 
                                    fontSize: 15,
                                    padding: 15, 
                                    fontWeight: 'bold', 
                                    fontFamily: 'sans-serif', 
                                    color: '#000000' }}
                            >
                                Agora você deve cadastrar sua 
                                empresa para dar início as atividades.
                                Dúvidas? Acesse nossa págins de tutoriais:
                                http://www.entregueapp.com/tutoriais
                            </Text>
                        </Container>
                        <Container 
                            style={{  
                                justifyContent: 'flex-end', 
                                alignItems: 'center',
                                flex: 1,
                                marginTop: 85,
                                marginHorizontal: 25, 
                                marginBottom: 20 }}
                        >    
                            <Button 
                                full rounded light icon='save'
                                onPress={() => { Actions.login(); }} 
                            >
                                <Text>Fazer Login</Text>
                            </Button>
                        </Container>
                </Container>
        </ImageBackground>
        );
    }
}
