import {
    CHANGE_EMAIL,
    CHANGE_PASSWORD,
    USER_LOGIN_SUCESS,
    USER_LOGIN_ERROR,
    LOGIN_IN_PROGRESS
} from '.././actions/types';

const INITIAL_STATE = {
    email: '',
    password: '',
    login_error: '',
    loading_login: false,

};

export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_EMAIL:
            return { ...state, email: action.payload };
        
        case CHANGE_PASSWORD:
            return { ...state, password: action.payload };

        case USER_LOGIN_SUCESS:
            return { ...state, ...INITIAL_STATE };

        case USER_LOGIN_ERROR:
            return { ...state, login_error: action.payload, loading_login: false };
            
        case LOGIN_IN_PROGRESS:
            return { ...state, loading_login: true };    

        default: 
            return state;
    }
};

