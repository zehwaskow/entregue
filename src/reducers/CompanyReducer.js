import {
    CHANGE_USER_EMAIL,
    CHANGE_COMPANY_EMAIL,
    CHANGE_USER_PASSWORD,
    CHANGE_USER_TYPE,
    CHANGE_USER_NAME,
    CHANGE_EIN,
    CHANGE_STATE_SUBSCRIPTION,
    CHANGE_COMPANY_NAME,
    CHANGE_ZIP_CODE,
    CHANGE_STREET,
    CHANGE_NUMBER,
    CHANGE_ADRESS_LINE_TWO,
    CHANGE_NEIGHBORHOOD,
    CHANGE_CITY,
    CHANGE_STATE,
    CHANGE_LANDLINE_TELEPHONE,
    CHANGE_CELL_PHONE,
    CHANGE_WHATSAPP_PHONE,
    CHANGE_LEGAL_REPRESENTATIVE,
    USER_REGISTER_SUCESS,
    USER_REGISTER_ERROR,
    USER_LOGIN_SUCESS,
    USER_LOGIN_ERROR,
    LOGIN_IN_PROGRESS,
    REGISTER_IN_PROGRESS

} from '.././actions/types';

const INITIAL_STATE = {
    email: '',
    userType: '',
    companyEmail: '',
    password: '',
    name: '',
    ein: '',
    stateSubscription: '',
    companyName: '',
    zipCode: '',
    street: '',
    number: '',
    adressLineTwo: '',
    neighborhood: '',
    city: '',
    state: '',
    landlineTelephone: '',
    cellPhone: '',
    whatsappPhone: '',
    legalRepresentative: '',
    register_error: '',
    login_error: '',
    loading_login: false,
    loading_register: false,
};

export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_USER_EMAIL:
            return { ...state, email: action.payload };
                
        case CHANGE_COMPANY_EMAIL:
                    return { ...state, companyEmail: action.payload };    
        case CHANGE_USER_PASSWORD:
            return { ...state, password: action.payload };

        case CHANGE_USER_TYPE:
            return { ...state, userType: action.payload };    

        case CHANGE_USER_NAME:
            return { ...state, name: action.payload };    

        case CHANGE_EIN:
            return { ...state, ein: action.payload };   

        case CHANGE_STATE_SUBSCRIPTION:
            return { ...state, stateSubscription: action.payload };   
    
        case CHANGE_COMPANY_NAME:
            return { ...state, companyName: action.payload };   

        case CHANGE_ZIP_CODE:
            return { ...state, zipCode: action.payload };   

        case CHANGE_STREET:
            return { ...state, street: action.payload };   

        case CHANGE_NUMBER:
            return { ...state, number: action.payload };   

        case CHANGE_ADRESS_LINE_TWO:
            return { ...state, adressLineTwo: action.payload };   

        case CHANGE_NEIGHBORHOOD:
            return { ...state, neighborhood: action.payload }; 
        
        case CHANGE_CITY:
            return { ...state, city: action.payload };       

        case CHANGE_STATE:
            return { ...state, state: action.payload }; 
            
        case CHANGE_LANDLINE_TELEPHONE:
            return { ...state, landlineTelephone: action.payload }; 

        case CHANGE_CELL_PHONE:
            return { ...state, cellPhone: action.payload };   
            
        case CHANGE_WHATSAPP_PHONE:
            return { ...state, whatsappPhone: action.payload };   
        
            case CHANGE_LEGAL_REPRESENTATIVE:
            return { ...state, legalRepresentative: action.payload };

        case USER_REGISTER_SUCESS: 
            return { ...state,     
                email: '',
                companyEmail: '',
                password: '',
                userType: '',
                name: '',
                ein: '',
                stateSubscription: '',
                companyName: '',
                zipCode: '',
                street: '',
                number: '',
                adressLineTwo: '',
                neighborhood: '',
                city: '',
                state: '',
                landlineTelephone: '',
                cellPhone: '',
                whatsappPhone: '',
                legalRepresentative: '',
                register_error: '',
                login_error: '',
                loading_register: false, 
        };    

        case USER_REGISTER_ERROR:
            return { ...state, register_error: action.payload, loading_register: false };

        case USER_LOGIN_SUCESS:
            return { ...state, ...INITIAL_STATE };

        case USER_LOGIN_ERROR:
            return { ...state, login_error: action.payload, loading_login: false };
            
        case REGISTER_IN_PROGRESS:
            return { ...state, loading_register: true };
            
        case LOGIN_IN_PROGRESS:
            return { ...state, loading_login: true };    

        default: 
            return state;
    }
};

