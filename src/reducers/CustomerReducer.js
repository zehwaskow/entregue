import {
    CHANGE_CUSTOMER_EMAIL, 
    CHANGE_CUSTOMER_TYPE,
    CHANGE_CUSTOMER_NAME,
    CHANGE_CUSTOMER_EIN,
    CHANGE_CUSTOMER_STATE_SUBSCRIPTION,
    CHANGE_CUSTOMER_ZIP_CODE, 
    CHANGE_CUSTOMER_STREET, 
    CHANGE_CUSTOMER_NUMBER, 
    CHANGE_CUSTOMER_ADRESS_LINE_TWO,
    CHANGE_CUSTOMER_NEIGHBORHOOD,
    CHANGE_CUSTOMER_CITY,
    CHANGE_CUSTOMER_STATE,
    CHANGE_CUSTOMER_LANDLINE_TELEPHONE,
    CHANGE_CUSTOMER_CELL_PHONE,
    CHANGE_CUSTOMER_WHATSAPP_PHONE,
    CHANGE_CUSTOMER_LEGAL_REPRESENTATIVE,
    CHANGE_CUSTOMER_OBSERVATIONS,
    CUSTOMER_REGISTER_SUCCESS,
    CUSTOMER_REGISTER_ERROR,
    CUSTOMER_REGISTER_IN_PROGRESS

} from '.././actions/types';

const INITIAL_STATE = {
    customerType: '',
    customerEmail: '',
    customerName: '',
    customerEin: '',
    customerStateSubscription: '',
    customerZipCode: '',
    customerStreet: '',
    customerNumber: '',
    customerAdressLineTwo: '',
    customerNeighborhood: '',
    customerCity: '',
    customerState: '',
    customerLandlineTelephone: '',
    customerCellPhone: '',
    customerWhatsappPhone: '',
    customerLegalRepresentative: '',
    customer_register_error: '',
    customerObservations: '',
    loading_customer_register: false,
};

export default(state = INITIAL_STATE, action) => {
    switch (action.type) {
        case CHANGE_CUSTOMER_EMAIL:
            return { ...state, customerEmail: action.payload };
                
        case CHANGE_CUSTOMER_TYPE:
            return { ...state, customerType: action.payload };    

        case CHANGE_CUSTOMER_NAME:
            return { ...state, customerName: action.payload };    

        case CHANGE_CUSTOMER_EIN:
            return { ...state, customerEin: action.payload };   

        case CHANGE_CUSTOMER_STATE_SUBSCRIPTION:
            return { ...state, customerStateSubscription: action.payload };   

        case CHANGE_CUSTOMER_ZIP_CODE:
            return { ...state, customerZipCode: action.payload };   

        case CHANGE_CUSTOMER_STREET:
            return { ...state, customerStreet: action.payload };   

        case CHANGE_CUSTOMER_NUMBER:
            return { ...state, customerNumber: action.payload };   

        case CHANGE_CUSTOMER_ADRESS_LINE_TWO:
            return { ...state, customerAdressLineTwo: action.payload };   

        case CHANGE_CUSTOMER_NEIGHBORHOOD:
            return { ...state, customerNeighborhood: action.payload }; 
        
        case CHANGE_CUSTOMER_CITY:
            return { ...state, customerCity: action.payload };       

        case CHANGE_CUSTOMER_STATE:
            return { ...state, customerState: action.payload }; 
            
        case CHANGE_CUSTOMER_LANDLINE_TELEPHONE:
            return { ...state, landlineTelephone: action.payload }; 

        case CHANGE_CUSTOMER_CELL_PHONE:
            return { ...state, customerCellPhone: action.payload };   
            
        case CHANGE_CUSTOMER_WHATSAPP_PHONE:
            return { ...state, customerWhatsappPhone: action.payload };   
        
        case CHANGE_CUSTOMER_LEGAL_REPRESENTATIVE:
            return { ...state, customerLegalRepresentative: action.payload };
        
        case CHANGE_CUSTOMER_OBSERVATIONS:
            return { ...state, customerObservations: action.payload };

        case CUSTOMER_REGISTER_ERROR:
            return { ...state, customer_register_error: action.payload, loading_customer_register: false };

        case CUSTOMER_REGISTER_SUCCESS:
            return { ...state, 
                customerType: '',
                customerEmail: '',
                customerName: '',
                customerEin: '',
                customerStateSubscription: '',
                customerZipCode: '',
                customerStreet: '',
                customerNumber: '',
                customerAdressLineTwo: '',
                customerNeighborhood: '',
                customerCity: '',
                customerState: '',
                customerLandlineTelephone: '',
                customerCellPhone: '',
                customerWhatsappPhone: '',
                customerLegalRepresentative: '',
                customer_register_error: '',
                customerObservations: '',
                loading_customer_register: false,
            };
            
        case CUSTOMER_REGISTER_IN_PROGRESS:
            return { ...state, loading_customer_register: true };   

        default: 
            return state;
    }
};

