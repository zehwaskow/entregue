import { LIST_CUSTOMER } from '../actions/types';

const INITIAL_STATE = {};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case LIST_CUSTOMER:
            return action.payload;
        default:
            return state;

    }
};
