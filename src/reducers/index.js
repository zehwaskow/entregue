import { combineReducers } from 'redux';
import AuthenticationReducer from './AuthenticationReducer';
import CompanyReducer from './CompanyReducer';
import CustomerReducer from './CustomerReducer';

export default combineReducers({
    AuthenticationReducer,
    CompanyReducer,
    CustomerReducer,
});
